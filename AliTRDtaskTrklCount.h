#ifndef AliTRDtaskTrklCount_cxx
#define AliTRDtaskTrklCount_cxx

#include <map>
#include <string>


// example of an analysis task creating a p_t spectrum
// Authors: Panos Cristakoglou, Jan Fiete Grosse-Oetringhaus, Christian Klein-Boesing

class TH1F;
class TH2F;
class TNtuple;
class AliESDEvent;

#include "AliAnalysisTaskSE.h"

class AliTRDtaskTrklCount : public AliAnalysisTaskSE {
 public:
  //AliTRDtaskTrklCount() : AliAnalysisTaskSE(), fESD(0), fOutputList(0), fHistPt(0) {}
  AliTRDtaskTrklCount(const char *name);
  virtual ~AliTRDtaskTrklCount() {}
  
  virtual void   UserCreateOutputObjects();
  virtual void   UserExec(Option_t *option);
  virtual void   Terminate(Option_t *);
  
 private:
  AliESDEvent *fESD;    //! ESD object
  AliTRDgeometry * fGeo; //! detector geometry
  TList       *fOutputList; //! Output list
  //TH1F        *fHistPt; //! Pt spectrum

  std::map<std::string, TH1*>     fHist; //! easy access to 1D histos
  std::map<std::string, TH2*>     fH2;   //! easy access to 2D histos
  std::map<std::string, TNtuple*> fNt;   //! easy access to ntuples

  Int_t fEvCount; //! event counter

  static const Int_t fNMcms = 540*8*16; //! number of 
  
  AliTRDtaskTrklCount(const AliTRDtaskTrklCount&); // not implemented
  AliTRDtaskTrklCount& operator=(const AliTRDtaskTrklCount&); // not implemented
  
  ClassDef(AliTRDtaskTrklCount, 1); // example of analysis
};

#endif
