#include "TChain.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"

#include "AliAnalysisTask.h"
#include "AliAnalysisManager.h"

#include "AliESDEvent.h"
#include "AliESDInputHandler.h"
#include "AliCentrality.h"

#include "AliMCEventHandler.h"
#include "AliMCEvent.h"

//#include "AliKalmanTrack.h"

#include "AliTRDpadPlane.h"
#include "AliTRDtrackV1.h"
#include "AliTRDseedV1.h"

#include "AliTRDtaskTrklCount.h"
#include "AliOstream.h"


#include <iostream>

using namespace std;

// example of an analysis task creating a p_t spectrum
// Authors: Panos Cristakoglou, Jan Fiete Grosse-Oetringhaus, Christian Klein-Boesing
// Reviewed: A.Gheata (19/02/10)

ClassImp(AliTRDtaskTrklCount)

//________________________________________________________________________
AliTRDtaskTrklCount::AliTRDtaskTrklCount(const char *name) 
: AliAnalysisTaskSE(name), fESD(0), fOutputList(0), fEvCount(-1)
{
  // Constructor

  // Define input and output slots here
  // Input slot #0 works with a TChain
  DefineInput(0, TChain::Class());
  // Output slot #0 id reserved by the base class for AOD
  // Output slot #1 writes into a TH1 container
  DefineOutput(1, TList::Class());

  // // the geometry could be created in the constructor or similar
  fGeo = new AliTRDgeometry;
  if (!fGeo) {
    cerr << "ERROR: cannot create geometry " << endl;
  }


}

//________________________________________________________________________
void AliTRDtaskTrklCount::UserCreateOutputObjects()
{
  // Create histograms
  // Called once

  fHist["err"] = new TH1F("fHistErrors",
			 "Errors;error number;N",
			 40, 0.5, 40.5);

  fHist["err"]->GetXaxis()->SetBinLabel(1, "det order");
  fHist["err"]->GetXaxis()->SetBinLabel(2, "row order");
  fHist["err"]->GetXaxis()->SetBinLabel(3, "col order");

  fHist["err"]->GetXaxis()->SetBinLabel(5, "MCM order");
  
  fHist["pt"] = new TH1F("fHistPt",
			 "P_{T} distribution;P_{T} (GeV/c);dN/dP_{T} (c/GeV)",
			 15, 0.1, 3.1);
  //fHist["pt"]->GetXaxis()->SetTitle("P_{T} (GeV/c)");
  //fHist["pt"]->GetYaxis()->SetTitle("dN/dP_{T} (c/GeV)");
  fHist["pt"]->SetMarkerStyle(kFullCircle);

  fHist["mult"] = new TH1F("fHistMult",
			 "Multiplicity;N_{tracks};N_{ev}",
			 40, 0., 20000.);

  fHist["trk"]  = new TH1F("fHistTrdTrk",
			   "Number of TRD Tracks;N_{tracks};N_{ev}",
			   40, 0., 2000.);

  fHist["trkl"] = new TH1F("fHistTrdTrkl",
			   "Number of TRD Tracklets;N_{tracklets};N_{ev}",
			   40, 0., 80000.);

  fHist["trklmcm"] = new TH1D("fHistTrdTrklPerMCM",
			   "Number of TRD Tracklets per MCM;N_{tracklets};N_{ev}",
			   11, -0.5, 10.5);

  fHist["trklmcmded"] = new TH1D("fHistTrdTrklPerMCMDedupl",
				 "Number of TRD Tracklets per MCM (after deduplication);N_{tracklets};N_{ev}",
			   11, -0.5, 10.5);

  fH2["trkl_mult"] = new TH2F("fHistTrdTrklMult",
			      "N_{tracklets} vs mult;N_{tracks};N_{TRD tracklets}",
			      40, 0., 20000., 40, 0., 80000.);
  
  fH2["trkl_corr"] = new TH2F("fHistCorr",
			      "Correlation;#Delta y;#Delta dy",
			      100, -0.5, 0.5, 100, -1.0, 1.0);


  //fNt["trkl"] = new TNtuple("fNtTrkl", "fNtTrkl", "ev:i:y:dy:z:hc:ly:col:mcm");
  fNt["mcm"]  = new TNtuple("fNtMcm",  "fNtMcm",  "ev:mcm:ntrkl");
  //fNt["trp"]  = new TNtuple("fNtTrkP", "fNtTrklP",
  //"ev:i:j:y1:dy1:y2:dy2:dy:ddy:dd:dupl");

  // Event ntuple - meaning of fields:
  //    ev        event number
  //    cent      centrality percentile
  //    ntr       number of ESD tracks
  //    ntrkl     number of TRD tracklets
  //    ntrklded  number of TRD tracklets after deduplication
  //    nhcavg    average number of tracklets per HC
  //    nhcrms    RMS of #tracklets/HC
  //    nhcmin    min #tracklets/HC
  //    nhcmax    max #tracklets/HC
  fNt["ev"]   = new TNtuple("fNtEvent", "fNtEvent",
			    "ev:cent:ntr:ntrkl:ntrklded:hcavg:hcrms:hcmin:hcmax");
  
  
  fOutputList = new TList();

  for (std::map<std::string, TNtuple*>::iterator itr = fNt.begin();
       itr != fNt.end(); itr++) {
    fOutputList->Add(itr->second);
  }
  
  for (std::map<std::string, TH1*>::iterator itr = fHist.begin();
       itr != fHist.end(); itr++) {
    
    fOutputList->Add(itr->second);
  }

  // fOutputList->Add(fHist["err"]);
  // fOutputList->Add(fHist["pt"]);
  // fOutputList->Add(fHist["mult"]);
  // fOutputList->Add(fHist["trkl"]);
  // fOutputList->Add(fHist["trklmcm"]);
  // fOutputList->Add(fHist["trk"]);

  fOutputList->Add(fH2["trkl_mult"]);
  fOutputList->Add(fH2["trkl_corr"]);

  
  PostData(1, fOutputList);

}

//________________________________________________________________________
void AliTRDtaskTrklCount::UserExec(Option_t *) 
{

  // Main loop
  // Called for each event

  fEvCount++; // increase event counter

  
  // -----------------------------------------------------------------
  // prepare event data structures
  fESD = dynamic_cast<AliESDEvent*>(InputEvent());
  if (!fESD) {
    printf("ERROR: fESD not available\n");
    return;
  }

  // -----------------------------------------------------------------
  // print some event information
  if (0) {
    cout << "    Triggers: " << fESD->GetFiredTriggerClasses() << endl;
    
    cout << "    ESD tracks:  " << fESD->GetNumberOfTracks() << endl;
    
    cout << "    ESD friend:" << fESDfriend << endl;
    cout << "    # of friend tracks:   "
	 << fESDfriend->GetNumberOfTracks() << endl;
    
    cout << "    # of TRD tracks:     "
	 << fESD->GetNumberOfTrdTracks() << endl;
    cout << "    # of TRD tracklets:  "
	 << fESD->GetNumberOfTrdTracklets() << endl;
  }

  // -----------------------------------------------------------------
  // basic event cuts
  
  //if ( ! fESD->IsTriggerClassFired("CV0L7-B-NOPF-CENT") ) return;
  if ( ! fESD->IsTriggerClassFired("CINT7-B-NOPF-CENT") ) return;

  // approximate centrality cut: 10% most central
  //if ( fESD->GetNumberOfTracks() < 9500) return;
    
  cout << "    Event accepted" << endl;

  
  // -----------------------------------------------------------------
  // Loop over TRD tracklets

  Int_t nroc = 540;
  Int_t nmcm = nroc*128;
  
  vector<Double_t> trklcnt_hc(nroc*2); // array of tracklet counts
  vector<Double_t> trklcnt_raw(nmcm); // array of tracklet counts
  vector<Double_t> trklcnt_rej(nmcm); // array of tracklet counts

  Int_t ntrklded = 0;

  
  for (Int_t itrkl = 0; itrkl < fESD->GetNumberOfTrdTracklets(); itrkl++) {

    AliESDTrdTracklet trkl = *(fESD->GetTrdTracklet(itrkl));

    //----------------------------------------------------------------------
    // deduplication
    bool duplicate = false;
    for (Int_t jtrkl = (itrkl>10 ? itrkl-10 : 0); jtrkl < itrkl; jtrkl++) {

      AliESDTrdTracklet trkl2 = *(fESD->GetTrdTracklet(jtrkl));

      if (trkl.GetDetector() != trkl2.GetDetector()) continue;
      if (trkl.GetBinZ()   != trkl2.GetBinZ())   continue;

      Double_t dy = trkl.GetLocalY() - trkl2.GetLocalY();
      if (fabs(dy) > 3.0) continue;

      Double_t ddy = trkl2.GetDyDx() - trkl.GetDyDx();
      Double_t dd  = (trkl2.GetDyDx() - trkl.GetDyDx()) + 0.40*dy;

      if (dy > 0.2 && dy < 0.8 && dd>-0.01 && dd<0.03)
	duplicate = true;
      
      fH2["trkl_corr"]->Fill( trkl.GetLocalY() - trkl2.GetLocalY(), 
			      trkl.GetDyDx()   - trkl2.GetDyDx() );

      if (0) {
	fNt["trp"]->Fill(fEvCount, itrkl, jtrkl,
			 trkl.GetLocalY(), trkl.GetDyDx(),
			 trkl2.GetLocalY(), trkl2.GetDyDx(),
			 dy, ddy, dd, duplicate);
      }
      
      if (0) {
	cout << "[" << itrkl << "]  "
	     << trkl.GetLocalY() << " / " << trkl.GetDyDx()
	     << "  <-->  " 
	     << "[" << jtrkl << "]  "
	     << trkl2.GetLocalY() << " / " << trkl2.GetDyDx()
	     << endl;
      }

      
    }

    //----------------------------------------------------------------------
    // calculate and fill tracklet info

    Int_t col = fGeo->GetPadPlane(trkl.GetDetector())
      ->GetPadColNumber(trkl.GetLocalY());
    
    Int_t mcmnr = trkl.GetDetector()*128 + trkl.GetBinZ()*8 + col/18;

    trklcnt_raw[mcmnr]++;
    trklcnt_hc[trkl.GetHCId()]++;
    if (!duplicate) {
      trklcnt_rej[mcmnr]++;
      ntrklded++;
    }

    if (0) {
      fNt["trkl"]->Fill(fEvCount, itrkl,
			trkl.GetBinY(), trkl.GetBinDy(), trkl.GetBinZ(),
			trkl.GetHCId(), trkl.GetLocalY(),
			col, mcmnr
			); 
    }


  }

  // -----------------------------------------------------------------
  // calculate statistics of tracklets per ROC

  Double_t min=999999999., max=0., sum=0., sum2=0., nact = 0;
  for (Int_t i=0; i<2*nroc; i++) {
    Double_t x = trklcnt_hc[i];

    // ignore HCs with no entries
    if (x != 0) {
      nact++;
      sum += x;
      sum2 += x*x;
      if (x<min && x>=5) min = x;
      if (x>max) max = x;
    }
  }

  Double_t avg,rms;
  if (nact) {
    avg = sum/nact;
    rms = sqrt(sum2/nact - sum*sum/nact/nact);
  } else {
    avg = 0;
    rms = 0;
  }
    
  
  // -----------------------------------------------------------------
  // calculate statistics of tracklets per ROC
  fHist[ "mult" ]->Fill(fESD->GetNumberOfTracks());
  fHist[ "trkl" ]->Fill(fESD->GetNumberOfTrdTracklets());
  fHist[ "trk"  ]->Fill(fESD->GetNumberOfTrdTracks());
  
  fH2[ "trkl_mult" ]->Fill( fESD->GetNumberOfTracks(),
			    fESD->GetNumberOfTrdTracklets());

  
  
  fNt["ev"]->Fill( fEvCount,
		   fESD->GetCentrality()->GetCentralityPercentile("V0M"),
		   fESD->GetNumberOfTracks(),
		   fESD->GetNumberOfTrdTracklets(),
		   ntrklded,avg,rms,min,max);

  for (int i = 0; i<nmcm; i++) {
    fHist["trklmcm"] -> Fill( trklcnt_raw[i] );
    fHist["trklmcmded"] -> Fill( trklcnt_rej[i] );
  }
  
  //delete geo;
    
  PostData(1, fOutputList);
}      
//________________________________________________________________________
void AliTRDtaskTrklCount::Terminate(Option_t *) 
{

  // Draw result to the screen
  // Called once at the end of the query

//  fOutputList = dynamic_cast<TList*> (GetOutputData(1));
//  if (!fOutputList) {
//    printf("ERROR: Output list not available\n");
//    return;
//  }
//  
//  fHist["pt"] = dynamic_cast<TH1F*> (fOutputList->At(0));
//  if (!fHist["pt"]) {
//    printf("ERROR: fHistPt not available\n");
//    return;
//  }
//   
  
  //TCanvas *c1 = new TCanvas("AliTRDtaskTrklCount","Pt",10,10,510,510);
  //c1->cd(1)->SetLogy();
  //fHist["pt"]->DrawCopy("E");
}
