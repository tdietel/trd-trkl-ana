
#include <iostream>
#include <iomanip>


std::ostream& operator<< (std::ostream& os, const AliESDTrdTracklet& x) {

  Int_t det = x.GetDetector();
  Int_t sm = det/30;
  Int_t st = (det%30) / 5;
  Int_t ly = det%6;
  
  os << std::setfill('0')
     << "[" << std::setw(3) << det
     << " -> " << std::setw(2) << sm
     << "/" << std::setw(1) << st
     << "/" << std::setw(1) << ly << "]"
     << " r" << std::setw(2) << x.GetBinZ()
     << " y=" << x.GetLocalY()
     << " dy=" << x.GetBinDy();

  return os;
};

// Local Variables:
//   mode: c++
//     c-basic-offset: 4
//     indent-tabs-mode: nil
// End:

